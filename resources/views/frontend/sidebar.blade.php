<aside id="column-left" class="col-sm-3 ">
	<div class="box account info">
		<div class="box-heading">
			<h3>{{ trans('lang.account') }}</h3>
		</div>
		<div class="box-content">
			<ul>
				@if(Auth::check())
				<li>
					<a href="{{route('myaccount.index')}}">{{ trans('lang.my_account') }}</a>
				</li>
				<li>
					<a href="{{route('myaccount.edit',['myaccount' => Auth::user()->id])}}">{{ trans('lang.edit_account') }}</a>
				</li>
				<li>
					<a href="{{ route('wishlist.index') }}">{{ trans('lang.wish_list') }}</a>
				</li>
				<li>
					<a href="{{route('myaccount.edit.password',['myaccount' => Auth::user()->id])}}">Password</a>
				</li>
				<li>
					<a href="{{ route('authLogout') }}">{{ trans('lang.logout') }}</a>
				</li>
				@endif
			</ul>
		</div>
	</div>
</aside>