@extends('errors.error_layout')

@section('content')
    <div class="title">URL Not Found</div>
    <div>{{$message}} | {{$logTime}}</div>
@stop