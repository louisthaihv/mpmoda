@extends('errors.error_layout')

@section('content')
    <div class="title">Be right back.</div>
    <div>{{$message}} | {{$logTime}}</div>
@stop