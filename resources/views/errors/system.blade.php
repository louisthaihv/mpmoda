@extends('errors.error_layout')

@section('content')
    <div class="title"> Function Maintained. Be right back.</div>
    <div>{{$message}} | {{$logTime}}</div>
@stop