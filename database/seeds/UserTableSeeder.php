<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $data = 
        [
        	[
        		"name"=>"admin",
        		"email"=>"admin@admin.com",
        		"password"=>Hash::make('123456'),
        		"gender"=>1,
        		"role_id"=>1,
        		"birthdate" => "1990/25/11",
                "customer_id"=>null,

        	],
            [
                "name"=>"staff",
                "email"=>"staff@staff.com",
                "password"=>Hash::make('123456'),
                "gender"=>1,
                "role_id"=>1,
                "birthdate" => "1992/20/11",
                "customer_id"=>null,

            ],
            [
                "name"=>"customer1",
                "email"=>"customer1@cus.com",
                "password"=>Hash::make('123456'),
                "gender"=>1,
                "role_id"=>3,
                "birthdate" => "1992/20/11",
                "customer_id"=>1,

            ],
        	[
        		"name"=>"customer2",
        		"email"=>"customer2@cus.com",
        		"password"=>Hash::make('123456'),
        		"gender"=>1,
        		"role_id"=>3,
        		"birthdate" => "1992/20/11",
                "customer_id"=>2,

        	],

        ];
        User::insert($data);
    }
}
