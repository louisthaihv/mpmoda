<?php
define('CAT_NAME_COL', 'B');
define('PRO_SHORT_NAME_COL', 'C');
define('MODEL_COL', 'D');
define('SILK_COL', 'E');
define('COLOR_COL', 'F');
define('SIZE_COL', 'G');
define('PRO_CODE_COL', 'H');
define('PRO_LONG_NAME_COL', 'I');
define('PRO_PRICE_COL', 'J');
define('PRO_COUNTRY_COL', 'K');
define('PAGINATE', 12);