<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Silk extends Model
{
    protected $fillable = array('code');
}
