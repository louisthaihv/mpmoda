<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\WishProduct;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    /**
     * Constructor function.
     * Set global fro category all page
     **/
    public function __construct()
    {
        $count_wishlist = 0;
        $menu = '<ul class = "menu sf-menu">';
        $categories = Category::where('parrent_id', 0)->get();
        $menu = Category::getMenu($categories, $menu);
        $menu.='</ul>';
        if(Auth::check())
        {
            $count_wishlist = Auth::user()->wishList()->count();
        }
        
        \View::share('menu', $menu);
        \View::share('count_wishlist', $count_wishlist);
    }
}
