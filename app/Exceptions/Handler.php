<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use PDOException;
use Symfony\Component\Debug\Exception\FatalErrorException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        $this->logTime = date('Y-m-d H:i:s');
        $code = 'Code Error : ' . $e->getCode();
        $msg = $e->getMessage();
        $at = 'At : Line ' . $e->getLine() . 'File : ' . $e->getFile();
//        dd($e->getTraceAsString());
        if ($e instanceof ModelNotFoundException
            || $e instanceof PDOException
            || $e instanceof QueryException
            || $e instanceof FatalErrorException
            || $e instanceof NotFoundHttpException
        ) {
            return Log::error($code . ' -> ' . $msg . ' -> ' . $at);
        }
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (!env('APP_DEBUG')) {
            if ($e instanceof ModelNotFoundException
                || $e instanceof PDOException
                || $e instanceof QueryException
                || $e instanceof FatalErrorException
            ) {
                return \Response::view('errors.system', ['logTime' => $this->logTime, 'message' => $e->getMessage()]);
            }

            if($this->isHttpException($e))
            {
                switch ($e->getStatusCode()) {
                    // not found
                    case 404:
                        return \Response::view('errors.cus_404', ['logTime' => $this->logTime, 'message' => $e->getMessage()]);
                        break;
                    // internal error
                    case 500:
                        return \Response::view('errors.cus_500', ['logTime' => $this->logTime, 'message' => $e->getMessage()]);
                        break;
                    case 503:
                        return \Response::view('errors.cus_503', ['logTime' => $this->logTime, 'message' => $e->getMessage()]);
                        break;

                    default:
                        return $this->renderHttpException($e);
                        break;
                }
            }
        }
        return parent::render($request, $e);
    }
}
